var through = require('through')
var byline = require('byline')
var Stream = require('stream')

var fileVersionParser = function(strContent, then){


  var activeVersion = false;
  var versions = [];
  var versionStream = through(function write(data) {
      data = '' + data;
      if( !data.match(/^#/) && data.match(/[^/]+\/[^/]/) ){
        var version = data.split('/')[0];
        if(!activeVersion){
          this.emit('activeVersion', version);
          activeVersion = version;
        }
        this.queue(version);
        versions.push(version);
      }
    },
    function end () {
      this.queue(null);
      if(then)
        then(null, activeVersion, versions);
    });

  var streamContent = through();
  if(!(strContent instanceof Stream)){
    streamContent.pause();
    streamContent.queue(strContent);
    streamContent.queue(null);
    process.nextTick(function(){
      streamContent.resume();
    });
  }else{
    strContent.pipe(streamContent)
  }

  streamContent.pipe(byline()).pipe(versionStream);

  return versionStream;
};

module.exports = fileVersionParser;
