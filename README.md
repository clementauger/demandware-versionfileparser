# demandware-versionfileparser

Node module to parse `.version` file of demandware platform.

## Install

`npm i git+ssh://git@bitbucket.org:clementauger/demandware-versionfileparser.git --save`

## API

The module expose unique function which takes in arguments a `string` or a `stream`.

It optionally take a second argument `callback(err, string activeVersion, [string] versions )`

It returns a `stream` object (see dominictarr/through) that you can pipe out.

## Usage

### Streamable

It come with a stream API, declaring a new event

- on('activeVersion', function(activeVersionStr))

All versions found are emitted into `data` event.

```js
    versionFileParser( fs.createReadStream('...') )
      .on('activeVersion', function(d){
        // activeVersion
      })
      .on('data', function(d){
        // a version, can active one
      })
      .on('end', function(){
        // all done.
      })
```

### Callbackify

A more simple version using callback is also available.

```js
    versionFileParser( fs.createReadStream('...'), function(err, activeVersionFound, versionsFound){
      // all done
    })
```

## Notes

This module relies on `mocha` for testing.


