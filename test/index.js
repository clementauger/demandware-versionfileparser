require('should')
var fs = require('fs');
var path = require('path');
var pkg = require('../package.json');

var versionFileParser = require('../index.js');

describe('module ' + pkg.name, function(){

  var sampleFile = 'versionFileSample';
  var samplePath = path.join(__dirname, 'fixtures', sampleFile);

  it('can parse a string', function(done){
    var versionFileContent = fs.readFileSync(samplePath, 'utf8').toString();

    var activeVersionFound = '';
    var versionsFound = [];

    versionFileParser(versionFileContent)
      .on('activeVersion', function(d){
        activeVersionFound = d;
      })
      .on('data', function(d){
        versionsFound.push(d)
      })
      .on('end', function(){
        activeVersionFound.should.eql('version2')
        versionsFound.length.should.eql(2)
        versionsFound[0].should.eql('version2')
        versionsFound[1].should.eql('version1')
        done()
      })
  });

  it('can parse a stream', function(done){
    var versionFileContent = fs.createReadStream(samplePath);

    var activeVersionFound = '';
    var versionsFound = [];

    versionFileParser(versionFileContent)
      .on('activeVersion', function(d){
        activeVersionFound = d;
      })
      .on('data', function(d){
        versionsFound.push(d);
      })
      .on('end', function(){
        activeVersionFound.should.eql('version2')
        versionsFound.length.should.eql(2)
        versionsFound[0].should.eql('version2')
        versionsFound[1].should.eql('version1')
        done()
      })
  });

  it('can use a callback interface too', function(done){
    var versionFileContent = fs.createReadStream(samplePath);
    versionFileParser(versionFileContent, function(err, activeVersionFound, versionsFound){
      activeVersionFound.should.eql('version2')
      versionsFound.length.should.eql(2)
      versionsFound[0].should.eql('version2')
      versionsFound[1].should.eql('version1')
      done()
    })
  });

});
